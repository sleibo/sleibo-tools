#!/bin/sh

set -ue

OS="$2"

echo Setting up Openstack heat stack for CentOS $OS
STACK=$(./stack $*)
LOGIN=centos

case "$3" in
	5)
		salt_repo_version=2015.5-1
		;;
	8)
		salt_repo_version=2015.8-3.el${OS}
		;;
	*)
		echo Unknow Salt version
		;;
esac

log_file=stack-c${OS}.log

IP_list="$(nova list | grep "$STACK" |cut -f2 -d=| cut -f1 -d' ')"
IP_master="$(nova list | grep "$STACK" |grep saltmaster |cut -f2 -d=| cut -f1 -d' ')"

echo Setting up Salt on Master : $IP_master
ssh -tt ${LOGIN}@$IP_master "sudo -n yum install -y https://repo.saltstack.com/yum/redhat/salt-repo-${salt_repo_version}.noarch.rpm" >> $log_file 2>&1
ssh -tt ${LOGIN}@$IP_master 'sudo -n yum clean expire-cache' >> $log_file 2>&1
ssh -tt ${LOGIN}@$IP_master 'sudo -n yum install -y salt-master' >> $log_file 2>&1
ssh -tt ${LOGIN}@$IP_master "sudo -n mkdir -p /etc/salt/master.d && echo 'auto_accept: True' |sudo -n tee /etc/salt/master.d/scality.conf" >> $log_file 2>&1
ssh -tt ${LOGIN}@$IP_master 'sudo -n service salt-master start'  >> $log_file 2>&1

for ip in $IP_list
do
	echo Setting up Salt on $ip
	ssh -tt ${LOGIN}@$ip "sudo -n yum install -y https://repo.saltstack.com/yum/redhat/salt-repo-${salt_repo_version}.noarch.rpm" >> $log_file 2>&1 || true
	ssh -tt ${LOGIN}@$ip 'sudo -n yum clean expire-cache' >> $log_file 2>&1
	ssh -tt ${LOGIN}@$ip 'sudo -n yum install -y salt-minion' >> $log_file 2>&1
	ssh -tt ${LOGIN}@$ip "sudo -n mkdir -p /etc/salt/minion.d && echo master: $IP_master |sudo -n tee /etc/salt/minion.d/scality.conf" >> $log_file 2>&1
	ssh -tt ${LOGIN}@$ip 'sudo -n service salt-minion restart'  >> $log_file 2>&1
done


echo Setting up EPEL repository
ssh -tt ${LOGIN}@$IP_master "sudo -n salt '*' cmd.run 'yum install -y epel-release'" >> $log_file 2>&1
echo Setting up disks configuration
ssh -tt ${LOGIN}@$IP_master 'sudo -n salt "*node*" cmd.run "for dev in b c d e f g ; do echo 1 > /sys/block/vd\${dev}/queue/rotational; done ; for dev in h i ; do echo 0 > /sys/block/vd\${dev}/queue/rotational ; done ; lsblk -o NAME,ROTA"' >> $log_file 2>&1
#ssh -tt ${LOGIN}@$IP_master 'sudo salt "*" cmd.run "sed -i -e  's/# deb /deb /' /etc/apt/sources.list && apt-get update'
echo Your turn to work !

exec ssh -tt ${LOGIN}@${IP_master}
