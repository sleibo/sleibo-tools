#!/bin/sh

set -e

OS="$2".04

case $2 in
	12)
		OS=12.04
		OS_NICK=precise
		;;
	14)
		OS=14.04
		OS_NICK=trusty
		;;
	*) 	echo Unknown OS: $2
esac

echo Setting up Openstack heat stack for Ubuntu $OS
STACK=$(./stack $*)
LOGIN=ubuntu

case "$3" in 
	5) 
		salt_repo_version=2015.5
		;;
	8)
		salt_repo_version=2015.8
		;;
	*) 	echo Unsupported Salt version: $3
		;;
esac

log_file=stack-u${OS}.log

IP_list="$(nova list | grep "$STACK" |cut -f2 -d=| cut -f1 -d' ')"
IP_master="$(nova list | grep "$STACK" |grep saltmaster |cut -f2 -d=| cut -f1 -d' ')"

echo Setting up Salt on Master : $IP_master
ssh -tt ${LOGIN}@$IP_master "wget -O - https://repo.saltstack.com/apt/ubuntu/${OS}/amd64/${salt_repo_version}/SALTSTACK-GPG-KEY.pub | sudo -n apt-key add -"  >> $log_file 2>&1
ssh -tt ${LOGIN}@$IP_master "echo deb http://repo.saltstack.com/apt/ubuntu/${OS}/amd64/${salt_repo_version} ${OS_NICK} main | sudo -n tee /etc/apt/sources.list.d/saltstack.list" >> $log_file 2>&1
ssh -tt ${LOGIN}@$IP_master 'sudo -n apt-get update -y' >> $log_file 2>&1 || true 
ssh -tt ${LOGIN}@$IP_master 'sudo -n apt-get install -y salt-master' >> $log_file 2>&1 || true 
ssh -tt ${LOGIN}@$IP_master "sudo -n mkdir -p /etc/salt/master.d && echo 'auto_accept: True' |sudo -n tee /etc/salt/master.d/scality.conf" >> $log_file 2>&1
ssh -tt ${LOGIN}@$IP_master 'sudo -n service salt-master restart'  >> $log_file 2>&1  

for ip in $IP_list
do
	echo Setting up Salt on $ip
	ssh -tt ${LOGIN}@$ip "wget -O - https://repo.saltstack.com/apt/ubuntu/${OS}/amd64/${salt_repo_version}/SALTSTACK-GPG-KEY.pub | sudo -n apt-key add -" >> $log_file 2>&1
	ssh -tt ${LOGIN}@$ip "echo deb http://repo.saltstack.com/apt/ubuntu/${OS}/amd64/${salt_repo_version} ${OS_NICK} main | sudo -n tee /etc/apt/sources.list.d/saltstack.list"  >> $log_file 2>&1
	ssh -tt ${LOGIN}@$ip 'sudo -n apt-get update -y'  >> $log_file 2>&1 || true 
	ssh -tt ${LOGIN}@$ip 'sudo -n apt-get install -y salt-minion' >> $log_file 2>&1 || true
	ssh -tt ${LOGIN}@$ip "sudo -n mkdir -p /etc/salt/minion.d && echo master: $IP_master |sudo -n tee /etc/salt/minion.d/scality.conf" >> $log_file 2>&1
	ssh -tt ${LOGIN}@$ip 'sudo -n service salt-minion restart'  >> $log_file 2>&1 
done


echo Setting up Multiverse repository
ssh -tt ${LOGIN}@$IP_master "sudo -n salt '*' cmd.run 'sed -i -e \"/multiverse/s/\# deb /deb /\" /etc/apt/sources.list'" >> $log_file 2>&1
echo Setting up disks configuration
ssh -tt ${LOGIN}@$IP_master 'sudo -n salt "*node*" cmd.run "for dev in b c d e f g ; do echo 1 > /sys/block/vd\${dev}/queue/rotational; done ; for dev in h i ; do echo 0 > /sys/block/vd\${dev}/queue/rotational ; done ; lsblk -o NAME,ROTA"' >> $log_file 2>&1
echo Your turn to work !

exec ssh -tt ${LOGIN}@${IP_master}
