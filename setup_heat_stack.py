""" Creates an Heat Openstack stack with Salt preinstalled.
    The IP address of the salt master is displayed on stdout.

    OpenStack parameters can be given either on the command line or
    through standard environment variables :
        OS_PASSWORD
        OS_AUTH_URL
        OS_USERNAME
        OS_TENANT_NAME
        and a specific one OS_HEAT_ENDPOINT
"""

import argparse
from roos import heat
import yaml
import os
import sys


def main():
    """ Parses command line options and calls HeatClient to create the
    stack. """
    parser = argparse.ArgumentParser()

    parser.add_argument("--stack-name", required=True,
                        help="Stack name known by Openstack Heat")
    parser.add_argument("--os-password", default=os.getenv('OS_PASSWORD'))
    parser.add_argument("--os-auth-url", default=os.getenv('OS_AUTH_URL'))
    parser.add_argument("--os-username", default=os.getenv('OS_USERNAME'))
    parser.add_argument("--os-tenant-name",
                        default=os.getenv('OS_TENANT_NAME'))
    parser.add_argument("--os-heat-endpoint",
                        default=os.getenv('OS_HEAT_ENDPOINT'))

    # Add arguments from the Heat template

    with open(heat.get_default_template()) as param_file:
        params = yaml.load("\n".join(param_file.readlines()))

    for parameter in params['parameters'].keys():
        parser.add_argument(
            "--%s" % parameter,
            help=params['parameters'][parameter]['description'],
            default=params['parameters'][parameter].get('default', None))

    args = parser.parse_args()

    # Extract Heat templates vars
    tmpl_args = {key: vars(args)[key] for key in params['parameters'].keys()}

    try:
        heatc = heat.HeatClient(
            endpoint=args.os_heat_endpoint,
            auth_url=args.os_auth_url,
            username=args.os_username,
            password=args.os_password,
            tenant=args.os_tenant_name)
        heatc.connect()
        stack = heatc.create_stack(
            name=args.stack_name,
            poll=True,
            parameters=tmpl_args)
        ip_addr = stack.outputs[u'saltmaster'][u'ip']

        print ip_addr
        return 0
    except heat.HeatError as heat_error:
        print heat_error
        return 1
    except heat.TimeoutError:
        print "Timeout while creating stack"
        return 2
    except KeyError:
        print "Unable to retrieve supervisor IP address"
        return 3


if __name__ == "__main__":
    retval = main()
    sys.exit(retval)
