IP_master="$1"
NUMBER="${2:-9}"
LOGIN="${3:-centos}"

ssh -tt ${LOGIN}@192.168.15.$IP_master 'sudo -n yum install -y https://repo.saltstack.com/yum/redhat/salt-repo-2015.8-3.el6.noarch.rpm'
ssh -tt ${LOGIN}@192.168.15.$IP_master 'sudo -n yum clean expire-cache'
ssh -tt ${LOGIN}@192.168.15.$IP_master 'sudo -n yum install -y salt-master'
ssh -tt ${LOGIN}@192.168.15.$IP_master 'sudo -n yum install -y salt-minion'
ssh -tt ${LOGIN}@192.168.15.$IP_master 'sudo -n mkdir -p /etc/salt/minion.d && echo master: 192.168.15.28 |sudo -n tee /etc/salt/minion.d/scality.conf' 
ssh -tt ${LOGIN}@192.168.15.$IP_master 'sudo -n service salt-master start' 
ssh -tt ${LOGIN}@192.168.15.$IP_master 'sudo -n service salt-minion start' 

for ip in $(seq $((IP_master + 1)) $(( IP_master + NUMBER - 1))) ; do
 do 
	ssh -tt ${LOGIN}@192.168.15.$ip 'sudo -n yum install -y https://repo.saltstack.com/yum/redhat/salt-repo-2015.8-3.el6.noarch.rpm'
	ssh -tt ${LOGIN}@192.168.15.$ip 'sudo -n yum clean expire-cache'
	ssh -tt ${LOGIN}@192.168.15.$ip 'sudo -n yum install -y salt-minion'
	ssh -tt ${LOGIN}@192.168.15.$ip 'sudo -n mkdir -p /etc/salt/minion.d && echo master: 192.168.15.28 |sudo -n tee /etc/salt/minion.d/scality.conf' 
	ssh -tt ${LOGIN}@192.168.15.$ip 'sudo -n service salt-minion restart' 
 done

ssh -tt ${LOGIN}@192.168.15.$IP_master sudo -n salt-key -Ay



sudo salt-key -Ay
ssh -tt ${LOGIN}@192.168.15.$IP_master sudo -n salt '*node*' cmd.run 'for dev in b c d e f g ; do echo 1 > /sys/block/vd${dev}/queue/rotational; done ; for dev in h i ; do echo 0 > /sys/block/vd${dev}/queue/rotational ; done ; lsblk -o NAME,ROTA'
ssh -tt ${LOGIN}@192.168.15.$IP_master sudo salt '*' cmd.run "sed -i -e  's/# deb /deb /' /etc/apt/sources.list && apt-get update"
EOF
