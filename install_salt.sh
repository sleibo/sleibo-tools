#!/bin/bash

IP_master="$1"
NUMBER="${2:-9}"
SALT_VERSION="${3:-2015.8.10}"
LOGIN="${4:-ubuntu}"

ssh -tt ${LOGIN}@192.168.15.${IP_master} <<EOF
wget -o - https://bootstrap.saltstack.com | sudo -n sh -s -- -A 192.168.15.${IP_master} -M git v${SALT_VERSION}
sudo mkdir -p /etc/salt/master.d
echo 'auto-accept: True' | sudo tee /etc/salt/master.d/auto_accept.conf
sudo service salt-master restart
EOF

for ip in $(seq $((IP_master + 1)) $(( IP_master + NUMBER - 1))) ; do
    ssh -tt ${LOGIN}@192.168.15.${ip} "wget -o - https://bootstrap.saltstack.com | sudo -n sh -s -- -A 192.168.15.${IP_master} git v${SALT_VERSION}" &
done

wait

sudo salt-key -Ay
sudo salt '*node*' cmd.run 'for dev in b c d e f g ; do echo 1 > /sys/block/vd${dev}/queue/rotational; done ; for dev in h i ; do echo 0 > /sys/block/vd${dev}/queue/rotational ; done ; lsblk -o NAME,ROTA'
salt '*' cmd.run "sed -i -e  's/# deb /deb /' /etc/apt/sources.list && apt-get update"
EOF
