NUMBER=$1
shift
STACK=$(./stack $*)
LOGIN="ubuntu"

IP_list="$(nova list | grep "$STACK" |cut -f2 -d=| cut -f1 -d' ')"
IP_master="$(nova list | grep "$STACK" |grep saltmaster |cut -f2 -d=| cut -f1 -d' ')"

ssh -tt ${LOGIN}@$IP_master 'sudo -n wget -O - https://repo.saltstack.com/apt/ubuntu/16.04/amd64/latest/SALTSTACK-GPG-KEY.pub | sudo apt-key add -'
ssh -tt ${LOGIN}@$IP_master 'echo deb http://repo.saltstack.com/apt/ubuntu/14.04/amd64/2015.8 trusty main | sudo -n tee /etc/apt/sources.list.d/saltstack.list'
ssh -tt ${LOGIN}@$IP_master 'sudo -n apt-get update -y'

ssh -tt ${LOGIN}@$IP_master 'sudo -n apt-get install -y salt-master'

for ip in $IP_list
do
	ssh -tt ${LOGIN}@$ip 'sudo -n wget -O - https://repo.saltstack.com/apt/ubuntu/16.04/amd64/latest/SALTSTACK-GPG-KEY.pub | sudo apt-key add -'
	ssh -tt ${LOGIN}@$ip 'echo deb http://repo.saltstack.com/apt/ubuntu/14.04/amd64/2015.8 trusty main | sudo -n tee /etc/apt/sources.list.d/saltstack.list'
	ssh -tt ${LOGIN}@$IP_master 'sudo -n apt-get update -y'
ssh -tt ${LOGIN}@$IP_master 'sudo -n apt-get install -y salt-minion'
	ssh -tt ${LOGIN}@$IP_master 'sudo -n service salt-master start' 
	ssh -tt ${LOGIN}@$ip "sudo -n mkdir -p /etc/salt/minion.d && echo master: $IP_master |sudo -n tee /etc/salt/minion.d/scality.conf"
	ssh -tt ${LOGIN}@$ip 'sudo -n service salt-minion restart' 
done

ssh -tt ${LOGIN}@$IP_master sudo -n salt-key -Ay
ssh -tt ${LOGIN}@$IP_master 'sudo -n salt '*node*' cmd.run "for dev in b c d e f g ; do echo 1 > /sys/block/vd${dev}/queue/rotational; done ; for dev in h i ; do echo 0 > /sys/block/vd${dev}/queue/rotational ; done ; lsblk -o NAME,ROTA"'
#ssh -tt ${LOGIN}@$IP_master sudo salt '*' cmd.run "sed -i -e  's/# deb /deb /' /etc/apt/sources.list && apt-get update"
exec ssh -tt ${LOGIN}@$IP_master
